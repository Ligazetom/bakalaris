\LoadClassWithOptions{book}

\RequirePackage{mathtools}

\usepackage[utf8]{inputenc}
\usepackage[slovak]{babel}
\usepackage[IL2]{fontenc}

\RequirePackage{titlesec}
%\RequirePackage{epsdice}

\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{amsthm}

\RequirePackage{multirow}

\RequirePackage{tikz}
\usetikzlibrary{arrows,calc}
\usetikzlibrary{decorations.pathreplacing}

\RequirePackage{enumerate}
\RequirePackage{paralist}
\let\itemize\compactitem
\let\enumerate\compactenum
\plitemsep=4pt

\RequirePackage{booktabs}

\let\tmp\oddsidemargin
\let\oddsidemargin\evensidemargin
%\let\evensidemargin\tmp
\reversemarginpar

\RequirePackage{multicol}

\setlength{\parskip}{5pt}

\pagestyle{plain}

\newcounter{definicia}[chapter]
\newcounter{priklad}[chapter]
\newcounter{veta}[chapter]

\newcommand{\E}{\mathbb{E}}
\newcommand{\D}{\mathbb{D}}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\I}{\mathbb{I}}

\newcommand{\Pow}[1]{\mathrm{Pow}\left(#1\right)}
\newcommand{\card}[1]{\mathrm{card}\left(#1\right)}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\newcommand{\map}[2]{\mathrm{Map}\left(#1,#2\right)}

\DeclareMathOperator*{\argmin}{arg\,min}

\newcommand\defeq{\stackrel{\mathclap{\normalfont\mbox{\tiny\sffamily def}}}{=}}

\newcommand{\dohoda}{\noindent\textbf{Dohoda.}\quad}

\newcommand{\definicia}{\noindent\textbf{\stepcounter{definicia}Definícia \thechapter.\arabic{definicia}}\quad}

\newcommand{\priklad}{\noindent\textbf{\stepcounter{priklad}Príklad \thechapter.\arabic{priklad}}\quad}

\newcommand{\veta}{\noindent\textbf{\stepcounter{veta}Veta \thechapter.\arabic{veta}}\quad}

\newcommand{\implementacia}{\noindent\textbf{Implementácia.}\quad}

\newcommand{\uvodnaStranaPrace}{
\begin{center}
  \textbf{SLOVENSKÁ TECHNICKÁ UNIVERZITA V BRATISLAVE}
  
  \textbf{STAVEBNÁ FAKULTA}
\end{center}

Evidenčné číslo: \evidencneCislo

\vfill

\begin{center}
  {
    \LARGE
    \nazovPraceSK
  }
  
  \vspace{8pt}
  
  {
    \large
    \nazovPraceEN
  }
  
  \vspace{14pt}
  
  \bf
  Bakalárska práca
\end{center}

\vfill

\begin{tabbing}
  Študijný program: \hspace{38pt}\= Matematicko-počítačové modelovanie \\
  Číslo študijného odboru: \> 1114 \\
  Študijný odbor: \> 9.1.9 Aplikovaná matematika \\
  Školiace pracovisko: \> Katedra matematiky a deskriptívnej geometrie \\
  Vedúci bakalárskej práce: \> Ing.\ Adam Šeliga
\end{tabbing}

\vfill

{
  \bf
  \noindent
  Bratislava, 2019
  \hfill
  \autorPrace
}

%\cleardoublepage
}

\newcommand{\cestnePrehlasenie}{
  \null
  \vfill
  \noindent
  \textbf{Čestné prehlásenie}
  
  \vspace{14pt}
  \textPrehlasenie
  
  \vspace{14pt}
  \noindent
  Bratislava, \datumOdovzdaniaPrace
  \hfill
  ...........................
    
  \hfill
  \autorPrace\hspace{14pt}
  \cleardoublepage
}

\newcommand{\podakovanieVeducim}{
  \null
  \vfill
  \noindent
  \textbf{Poďakovanie}
  
  \vspace{14pt}
  \textPodakovanie
  \cleardoublepage
}
