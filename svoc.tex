\documentclass[10pt,a4paper]{text}

\setlength{\parindent}{0pt}
\DeclareMathOperator*{\argmax}{arg\,max}

\newcommand{\nazovPraceSK}{Nadbytočné a riedke reprezentácie \\ v spracovaní obrazu \\ \vspace{14pt}}
\newcommand{\nazovPraceEN}{}
\newcommand{\evidencneCislo}{SvF-???}
\newcommand{\autorPrace}{Juraj Krasňanský}
\newcommand{\datumOdovzdaniaPrace}{??.\ máj 2019}
\newcommand{\textPrehlasenie}{Vyhlasujem, že bakalársku prácu ,,Nadbytočné a riedke reprezentácie v spracovaní obrazu'' som vypracoval samostatne na základe použitia citovanej literatúry a s odbornou pomocou vedúceho práce.}
\newcommand{\textPodakovanie}{Nejaký text poďakovania}

\begin{document}

\pagestyle{empty}

\begin{center}
  \includegraphics[width=\linewidth]{figures/svf-header.png}
\end{center}

\hfill
Študentská vedecká konferencia

\hfill
Akademický rok: 2018/2019

\vfill

\begin{center}\LARGE
  Riedke a nadbytočné reprezentácie obrazu
\end{center}

\vfill

\begin{tabbing}
  \hspace*{5cm}\= \kill
  Meno študenta, ročník, odbor:
  \>
  Juraj Krasňanský, 3.\ ročník, MPM
  \\[5pt]
  Vedúci práce:
  \>
  Ing.\ Adam Šeliga
  \\[5pt]
  Katedra/ústav:
  \>
  Katedra matematiky a deskriptívnej geometrie
\end{tabbing}

\vfill

\begin{center}
  Bratislava, 27.\ apríla 2019
\end{center}

\newpage




\section*{Abstrakt}

V tejto práci sa zaoberáme riedkymi a nadbytočnými reprezentáciami signálu. Špeciálne sa venujeme základným úlohám tejto oblasti a ich riešenia pomocou aproximačných, alebo tiež greedy, algoritmov. Popisujeme dva aproximačné algoritmy, ktorých názov je matching-pursuit a orthogonal matching-pursuit algoritmus. V aplikačnej časti upravujeme tieto algoritmy zavedením pojmu podobnosti obrazu a skúšaním rôznych alternatív pre klasicky používanú kosínusovú podobnosť.

\vfill

\section*{Abstract}

In this work, we are interested in sparse and redundant representations of signals. Specially, we introduce basic problems from this field and methods for finding solutions using approximation, also called greedy, algorithms. We present two approximation algorithms, namely matching-pursuit and orthogonal matching-pursuit algorithms. In application part of this work we modify these algorithms by introducing so-called image similarity and testing other alternatives to classically used cosine similarity.

\vfill\vfill
\cleardoublepage

\frontmatter
{
\setlength{\parskip}{2pt}
\tableofcontents
}

\pagestyle{plain}
\mainmatter

\chapter{Spracovanie obrazu}

\section{Čo je spracovanie obrazu?}

Táto sekcia je z veľkej časti inšpirovaná publikáciami \cite{uvodKniha,mozno}.

Obraz môže byť definovaný ako dvojdimenzionálna funkcia $f(x,y)$, kde $x$ a $y$ sú priestorové súradnice a hodnoty funkcie $f$ nazývame intenzitou šedosti obrazu pre dané súradnice. Ak $x,y \text{ a } f$ nadobúdajú hodnoty z konečných množín, hovoríme o takzvanom digitálnom obraze. Spracovať digitálny obraz znamená spracovať ho pomocou digitálneho zariadenia. Je treba si uvedomiť, že digitálny obraz je zložený z konečného množstva elementov, z ktorého každý má danú polohu a hodnotu intenzity. Tieto elementy nazývame buď obrazové elementy, alebo pixely.

Keďže zrak je našim najvyvinutejším zmyslom, tak nie je prekvapivé, že práve obrazy hrajú tak dôležitú úlohu pri vnímaní. Avšak narozdiel od ľudí, ktorí vnímajú len určitý relatívne malý interval elektromagnetického spektra, zariadenia na spracovanie obrazu pokrývajú skoro celú jeho škálu vlnení už od gamma žiarenia až po rádiové vlny. \textbf{Tieto zariadenia sú schopné vykonávať úlohy na~obrazoch vygenerované zdrojmi, ktoré si človek s pojmom obraz nespája.} Medzi ne sa zaraďuje ultrazvuk, elektrónová mikroskopia a počítačom generované obrazy. Teda už  teraz si môžeme všimnúť, že spracovanie obrazu zahŕňa širokú a rôznorodú škálu využití a aplikácií.

Neexistuje žiadna všeobecná dohoda, ktorá by určovala hranice medzi jednotlivými oblasťami spracovania obrazu. Nie je teda stanovené kde končí analýza obrazu a začína strojové videnie. Avšak, môžeme si toto kontinuum rozdeliť na tri typy procesov v závislosti od ich komplexnosti. A to nízko-, stredo- a vysokoúrovňové procesy. Nízkoúrovňové procesy zahŕňajú operácie ako predspracovanie obrazu na odstránenie šumu, zvýraznenie kontrastu, alebo zaostrenie obrazu. Hlavná charakteristika nízkoúrovňového procesu je, že jeho vstupom, rovnako ako aj jeho výstupom, je obraz. Stredoúrovňové procesy spracovania obrazu zahŕňajú úlohy ako \textbf{segmentácia} -- rozdelenie obrazu na regióny alebo objekty, vyjadrenie týchto objektov vo forme vhodnej na ďalšie spracovanie počítačom a následné rozpoznávanie a triedenie jednotlivých objektov. Charakteristikou stredoúrovňových procesov je, že vo všeobecnosti im na vstup prichádzajú obrazy, no výstupom sú vlastnosti získané z daného obrazu ako napríklad hrany, kontúry, charakterizácia typu objektov v obraze sa nachádzajúcich a pod. Vysokoúrovňové procesy majú za úlohu vidieť obraz ako celok, vyvodiť rôzne logické prepojenia medzi jednotlivými objektami a rôzne ďalšie úlohy normálne spájané s videním.

\section{História spracovania obrazu}

Jednou z prvých aplikácií použitia digitálneho obrazu bolo v novinárskom odvetví, keď obrazy boli poslané podmorským káblom z Londýna do New Yorku. Zavedením Bartlaneovho káblového systému pre prenášanie obrazov v roku 1920, sa čas potrebný pre prenos obrazu cez Atlantický oceán skrátil z viac než jedného týždňa na menej než tri hodiny.

Jedným z prvých problémov spojeným s vizuálnou kvalitou týchto skorých digitálnych obrazov bol správny výber procedúr používaných pri tlači ako aj správne rozdelenie intenzít farby. Na začiatku bol Bartlaneov systém schopný zakódovať päť rôznych intenzít šedej. V roku 1929 sa tento počet zvýšil z päť na pätnásť. 

Pri vyššie spomenutých technológiách nebol nikde použitý digitálny počítač, takže nehovoríme o nich ako o digitálnom spracovaní obrazu. Keďže digitálny obraz vyžaduje dostatok úložného priestoru a dostatočne výkonnú výpočtovú silu, pokrok v tejto oblasti pevne závisel na pokroku a dostupnosti výpočtových technológií.

Dnešným počítačom predchádzala séria kľúčových vynálezov a rozhodnutí k~dosiahnutiu dostatočného výkonu, aby mohli byť použité pre digitálne spracovanie obrazu. Základ vznikol v roku 1940, kedy John von Neumann predstavil dva hlavné koncepty a to pamäť na uloženie samotného programu a dát ako aj vetvenie na základe podmienok. Tento prístup bol základom pre moderné procesory (CPU). Ďalej nasledovalo vynájdenie tranzistoru, vývoj vysokoúrovňových programovacích jazykov, vynájdenie integrovaných obvodov, vývoj operačných systémov, chvíľu na to vývoj mikroprocesorov, príchod prvých osobných počítačov a následne postupná miniaturizácia jednotlivých komponentov. Zarovno s nimi sa napredovalo aj v oblastiach úložných a zobrazovacích systémov, ktoré sú rovnako dôležité pre digitálne spracovanie obrazu.

Prvé počítače schopné vykonávať významné úlohy v oblasti spracovania obrazu sa objavili v šesťdesiatych rokoch, kedy sa začali používať na spracovanie snímok z vesmírnych družíc. V roku 1964 sa použil počítač na opravu obrazu, ktorý bol prenesený z družice Ranger 7 na Zem a obsahoval rôzne typy deformácií (distortion-preklad). O niečo neskôr, v neskorých šestdesiatych až skorých sedemdesiatych rokoch sa začali používať rôzne techniky spracovania obrazu aj v~oblasti zdravotníctva. Vynájdenie \textbf{computerized axial tomography (CAT)} v sedemdesiatych rokoch, sa považuje za jednu z najdôležitejších udalostí v aplikácii digitálneho spracovania obrazu v medicíne. 

Od šesťdesiatych rokov až do súčastnosti oblasť digitálneho spracovania obrazu významne narástla. Procedúry digitálneho spracovania obrazu si tiež našli využitie vo výrobných procesoch a biologických vedách, kde buď zvýšením kontrastu alebo zafarbením rôznych hodnôt intenzít obrazu sa zjednoduší interpretácia informácie obsiahnutej v röntgenovej snímke. Podobný prístup sa používa aj pri študovaní znečistenia ovzdušia zo satelitných snímok. Svoje miesto si našli tiež aj v archeológii pri restorácii obrazov, ktoré boli poslednými pozostatkami dávno stratených alebo zničených relikvií. Rovnako aj v oblasti fyziky, kde sa vykonávajú experimenty, ktoré sú veľmi náročné na zopakovanie a teda je dôležité získať z ich výsledkov čo najviac informácií. 

Všetky tieto príklady boli aplikácie digitálneho spracovania obrazu, kde výsledok bol určený pre vnímanie človekom. Na druhom konci spektra sídlia metódy a aplikácie, ktorých výsledok je určený pre ďalšie spracovanie počítačmi. Tieto výsledky majú málokedy podobnosť s vizuálnymi prvkami, ktoré ľudia vnímajú ako informáciu obsiahnutú v obraze. Príklady takýchto výsledkov sú štatistické momenty, koeficienty Fourierovej transformácie, viacpriestorové normy a iné. Typickými problémami v oblasti strojového videnia sú automatické rozpoznávanie znakov písma, odtlačkov prstov, sietnic, vzoriek krvi ako aj automatické spracovanie satelitných snímok pre predpovede počasia.

S postupným poklesom pomeru ceny počítačov k ich výpočtovému výkonu, rovnako ako aj s rozvojom sieťových a komunikačných technológií sa naskytli donedávna nevídané možnosti pre stály rast tejto oblasti.

\section{Riedke a nadbytočné reprezentácie}

V tejto sekcii čerpáme najmä z článkov autora Elad \cite{clanok1} a autorov Elad et al.\ \cite{clanok2}.

Základnou myšlienkou stojacou za týmto modelom, je nájsť čo možno najredšiu reprezentáciu vstupného obrazu, pomocou nadbytočného slovníku. 

Skvelou vlastnosťou tohto modelu je jeho univerzálnosť. Kvôli tejto vlastnosti si tento model našiel viacero uplatnení v spracovaní rôznych zdrojov obrazu.

\textbf{Aplikácie modelu}
 
Aplikácie tohto modelu sí vysoko efektívne pre rôzne úlohy, avšak každá z týchto aplikácií využíva model iným spôsobom.

\begin{itemize}
\item
 Odstránenie šumu z obrazu
 
 \item
 Zaostrovanie obrazu
 
 \item
 Dokreslovanie do obrazu
 
 \item
 Super rozlíšenie

\end{itemize}

\section[Matematický model reprezentácie pri zvolenom slovníku]{Matematický model reprezentácie \\ pri zvolenom slovníku}

\subsection{Opis modelu}

Cieľom nášho modelu je reprezentovať daný obraz pomocou lineárnych kombinácií obrazov nachádzajúcich sa v slovníku. Podmienky sa kladú na čo najmenší rozdiel medzi pôvodným a reprezentovaným obrazom, ako aj na čo najnižší počet členov tejto lineárnej kombinácie. Snažíme sa teda minimalizovať počet použitých obrazov zo slovníka, nutných na takéto reprezentovanie zadaného obrazu.

Inými slovami hľadáme taký vektor $x$, pre ktorý platí
\[
Dx=\sum_{i=1}^{d}x_i D_i= y,
\]
kde $D$ je náš slovník, $d$ je počet obrazov nachádzajúcich sa v slovníku a $y$ je náš vstupný obraz, ktorý chceme reprezentovať. Počet nenulových prvkov vo vektore $x$ označíme ako
\[
\norm{x}_0 = \lim_{p\to0} \norm{x}_{p}^{p} = \lim_{p\to0} \sum_{k=1}^{m} \lvert x_k\rvert^p = \mathrm{card}\,\{i : x_i \neq 0\}.
\]

\subsection{Náročnosť hľadania riešenia problému}

Úlohu môžeme teda prepísať do nasledujúceho tvaru:
\begin{center}
  \begin{tabular}{rl}
    minimalizuj & $\norm{x}_0$ \\
    za podmienky & $\norm{y-Dx}_2 \leq \delta$
  \end{tabular}
\end{center}
Inými slovami hľadáme vektor $x$ s najmenším možným počtom prvkom tak, aby rozdiel medzi reprezentáciou a pôvodným obrazom bol v $L_2$ norme nanajvýš $\delta$. Tento problém budeme nazývať problémom $P(D,y,\delta)$.

Takáto optimalizačná úloha je NP náročná. To znamená, že je možné pre dané riešenie overiť jeho správnosť v polynomiálnom čase, avšak nie je ho možné v~polynomiálnom čase nájsť.

Pre nájdenie riešenia takéhoto problému je potrebné nájsť čo najredší vektor $x$ taký, ktorý má mať čo najmenej nenulových koeficientov, pričom norma rozdielu jeho lineárnej kombinácie so stĺpcami slovníka $D$ a pôvodného obrazu má byť menšia ako nejaké vopred určené $\delta$. 

Riešiť takýto problém priamo hrubou silou je dosť neefektívne, nakoľko by bolo potrebné vyskúšať všetky možné kombinácie všetkých obrazov zo slovníka so všetkými možnými hodnotami koeficientov. Avšak za posledných pár rokov bolo navrhnutých zopár alternatívnych riešení takéhoto typu problémov pre získanie aproximačného riešenia. Niektoré sú založené na zvoľnení (relaxation) a nahrádzajú $\norm{x}_0$ normou $\norm{x}_1$, čím vzniká úloha
\begin{center}
  \begin{tabular}{rl}
    minimalizuj & $\norm{x}_1$ \\
    za podmienky & $\norm{y-Dx}_2 \leq \delta$
  \end{tabular}
\end{center}

Druhou triedou riešiacou problémy tohto typu využívajú princípy tzv. ,,greedy'' algoritmov, ktoré výsledné riešenie hľadajú iteračným postupom. Rovnako sa pri použití týchto algoritmov nahrádza pôvodná úloha $P(D,y,\delta)$ úlohou $P_0$ danou tvarom
\begin{center}
  \begin{tabular}{rl}
    minimalizuj & $\norm{y-Dx}_2$ \\
    za podmienky & $\norm{x}_0 \leq K$
  \end{tabular}
\end{center}

\subsection{Aproximačné algoritmy}

Informácie pre túto sekciu boli čerpané najmä z \cite{sparseKniha} a \cite{clanok4}.

Ako bolo vyššie spomenuté, na výpočet riešenia využívame aproximačné ,,greedy'' algoritmy. Sú to aproximačné algoritmy, ktoré v každom svojom kroku nájdu lokálne optimálne riešenie, no existuje šanca, že takýmto spôsobom nájdu aj globálne riešenie. Tieto typy algoritmov sa uplatňujú pri problémoch, kde je treba z určitej množiny objektov (v našom prípade slovníka obrazov), vybrať takú podmnožinu, ktorá spĺňa predom stanovenú vlastnosť a navyše má minimálne resp. maximálne ohodnotenie (strata informácie oproti originálnemu obrazu).

Konkrétne algoritmy, ktoré používame my sú matching-pursuit algoritmus a orthogonal matching-pursuit algoritmus. Oba algoritmy majú za úlohu nájsť v danej iterácii čo najpodobnejší obraz (alebo jeho kladný násobok) zo slovníka $D$ k reziduám. Reziduá aktualizujeme v každej iterácii ako normu rozdielu originálneho obrazu a aktuálnou reprezentáciou. Ako výsledok dostávame vektor koeficientov. Rozdiel medzi matching-pursuit algoritmom a orthogonal matching-pursuit algoritmom je v spôsobe ako daný vektor vzniká. 

Matching-pursuit algoritmus nezaujíma nič iné, než nájsť jeden koeficient, ktorý v danej iterácii rieši úlohu čo najlepšie. Na druhú stranu, orthogonal matching-pursuit zohľadňuje už vybraté koeficienty a to tak, že najlepšie riešenie hľadá nielen úpravou aktuálneho koeficientu, ale aj úpravou koeficientov získaných v predošlých iteráciách. 

\subsubsection*{Matching-pursuit algoritmus}

V tomto algoritme ukladáme indexy použitých obrazov zo slovníka $D$ do množiny $\Omega$. Tým pádom množina $\Omega^c$ označuje množinu zatiaľ nepoužitých indexov, ktorá je daná ako
\[
  \Omega^c
  =
  \{1,2,\dots,s\} \setminus \Omega,
\]
kde $s$ je počet obrazov v slovníku.

V každej iterácii volíme taký obraz, ktorý je najpodobnejší doteraz získaným rezíduám danej aproximácie. Tento výber robíme na základe kosínusovej podobnosti (viac v sekcii 3.2).

\subsubsection*{Orthogonal matching-pursuit algoritmus}

V tomto algoritme, rovnako ako v matching-pursuit algoritme, ukladáme indexy už použitých obrazov zo slovníka $D$ do množiny $\Omega$ a každý novo vybratý obraz vyberáme na základe podobnosti s reziduami, ale navyše po pridaní nového obrazu zo slovníka a teda aj nového koeficientu do vektoru $x$, znovu prepočítavame doposiaľ všetky získané koeficienty pomocou metódy najmenších štvorcov.

\begin{table}[p]
  \centering
  \begin{tabular}{ll} \toprule
    \textbf{úloha} & aproximácia riešenia úlohy $P_0$ \\ \midrule
    \textbf{vstupy} & matica $D \in \mathbb{R}^{n^2 \times d}$ \\
    & vektor $b \in \mathbb{R}^{...}$ \\
    & počet obrazov $L$ \\ \midrule
    \multicolumn{2}{l}{\textbf{inicializácia}} \\
    \multicolumn{2}{p{0.95\linewidth}}{
      \begin{itemize}
        \item
        polož $x^0 = \mathbf{0}$
        \item
        polož $r^0 = b - D\cdot x^0 = b$
        \item
        polož $\Omega = \emptyset$
      \end{itemize}
    } \\ \midrule
    \multicolumn{2}{l}{\textbf{iterácia $k=1,2,\dots,L$}} \\
    \multicolumn{2}{p{0.95\linewidth}}{
      \begin{itemize}
        \item
        nájdi index $j$ zo zatiaľ nepoužitých obrazov v slovníku
        \[
          j
          =
          \argmax_{i \in \Omega^c}
          \left|
            \frac{r^{k-1} \cdot D_i}{\norm{r^{k-1}} \norm{D_i}}
          \right|
        \]
        \item
        vypočítaj koeficient
        \[
          \alpha
          =
          \frac{r^{k-1}\cdot D_j}{\norm{r^{k-1}}\norm{D_j}}
        \]
        \item
        polož $x^k = x^{k-1} + \alpha D_j$
        \item
        polož $r^k = r^{k-1} - \alpha D_j$
      \end{itemize}
    } \\ \midrule
    \textbf{výstup} & aproximácia obrazu $x$ je vektor $x^k$
     \\ \bottomrule
  \end{tabular}
  
  \caption{Popísanie matching-pursuit algoritmu}
\end{table}

\begin{table}[p]
  \begin{tabular}{ll} \toprule
    \textbf{úloha} & aproximácia riešenia úlohy $P_0$ \\ \midrule
    \textbf{vstupy} & matica $D \in \mathbb{R}^{n^2 \times d}$ \\
    & vektor $b \in \mathbb{R}^{...}$ \\
    & počet obrazov $L$ \\ \midrule
    \multicolumn{2}{l}{\textbf{inicializácia}} \\
    \multicolumn{2}{p{0.95\linewidth}}{
      \begin{itemize}
        \item
        polož $x^0 = \mathbf{0}$
        \item
        polož $r^0 = b - D\cdot x^0 = b$
        \item
        polož $\Omega = \emptyset$
      \end{itemize}
    } \\ \midrule
    \multicolumn{2}{l}{\textbf{iterácia $k=1,2,\dots,L$}} \\
    \multicolumn{2}{p{0.95\linewidth}}{
      \begin{itemize}
        \item
        nájdi index $j$ zo zatiaľ nepoužitých obrazov v slovníku
        \[
          j
          =
          \argmax_{i \in \Omega^c}
          \left|
            \frac{r^{k-1} \cdot D_i}{\norm{r^{k-1}} \norm{D_i}}
          \right|
        \]
        \item
        vypočítaj vektor koeficientov
        \[
          \alpha
          =
          \left( D_\Omega^\intercal D_\Omega \right)^{-1} D_\Omega^\intercal x
        \]
        \item
        polož $x^k = \alpha D_\Omega$
        \item
        polož $r^k = x - x^k$
      \end{itemize}
    } \\ \midrule
    \textbf{výstup} & aproximácia obrazu $x$ je $x^k$
     \\ \bottomrule
  \end{tabular}
  
  \caption{Popísanie orthogonal matching-pursuit algoritmu}
\end{table}

\chapter{Miera podobnosti}

\section{Kosínusová podobnosť}

Majme dva vektory $a \in \mathbb{R}^n$ a $b \in \mathbb{R}^n$. Potom vzťah daný ako

\[
\frac{a \cdot b}{\norm{a}\norm{b}} 
=
\frac{\displaystyle\sum_{i=1}^n a_i b_i}{\displaystyle\sqrt{\sum_{i=1}^n a_i^2}\sqrt{\sum_{i=1}^n b_i^2}}
\]

nazývame kosínusovou podobnosťou vektorov $a$ a $b$. Hodnoty nadobúda v intervale $[-1,1]$, avšak v našom prípade, keďže nás nezaujíma či je podobnosť kladná alebo záporná, budeme za kosínusovú podobnosť uvažovať absolútnu hodnotu tohto čísla, ktorá sa pohybuje v intervale $[0,1]$ a kde nula značí kolmosť resp.\ nekoreláciu.

\section{Matematický popis miery podobnosti}

Ako už bolo popísané, pri matching-pursuit i orthogonal matching-pursuit algoritmoch vyberáme zatiaľ nepoužitý obraz zo slovníka pomocou absolútnej hodnoty kosínusovej transformácie. Toto zobrazenie môžeme chápať ako ,,mieru podobnosti''. Samozrejme, takýchto mier podobnosti môžeme uvažovať viacero a preto v tejto sekcii axiomaticky definujeme charakteristiku týchto zobrazení.

\textbf{Definícia 3.1.}
Zobrazenie $\rho \colon \mathbb{R}^n \times \mathbb{R}^n \to [0,1]$ nazveme mierou podobnosti, ak platia nasledujúce tri podmienky:
\begin{enumerate}[{(MP}1{)}]
  \item
  $\rho(x,x) = 1$ pre všetky $x \in \mathbb{R}^n$,
  \item
  $\rho(x,y) = \rho(y,x)$ pre všetky $x,y \in \mathbb{R}^n$ a
  \item
  $\rho(x,\lambda y) = \rho(x,y)$ pre všetky $x,y \in\mathbb{R}^n$ a $\alpha \in\mathbb{R}$.
\end{enumerate}

Hodnotu miery podobnosti teda vnímame ako číslo pochádzajúce z intervalu $[0,1]$ hovoriace o miere podobnosti uvažovaných obrazov. Ak sú obrazy dostatočne podobné, tak miera podobnosti sa blíži k hodnote $1$, ak sú takmer úplne odlišné, tak miera podobnosti sa blíži k číslu $0$.

Prečo očakávame práve tieto vlastnosti? Prvá vlastnosť, vlastnosť (MP1), hovorí o tom, že každý obraz je sám so sebou určite podobný. (MP2) hovorí zase o~tom, že nezáleží na tom, v akom poradí obrazov meriame ich podobnosť. Nakoniec, (MP3) hovorí o tom, že ak (rovnomerne) preškálujeme intenzity jedného z obrazov, tak to nemá žiaden vplyv na mieru podobnosti.

\textbf{Príklad 3.1.}
Všimnime si, že zobrazenie založené na kosínusovej podobnosti, teda zobrazenie
\[
  \mathrm{mp}_1(x,y)
  =
  \left|
    \frac{x \cdot y}{\norm{x} \norm{y}}
  \right|
\]
spĺňa všetky vlastnosti uvedené v definícii miery podobnosti.

V ďalších definíciách uvádzame zobrazenia založené na korelačných koeficientoch pochádzajúcich zo štatistiky, o ktorých neskôr ukážeme, že sú mierami podobnosti, teda spĺňajú vlastnosti (MP1) až (MP3) z definície 3.1.

\textbf{Definícia 3.2.}
Zobrazenie
\[
  \mathrm{mp}_2(x,y)
  =
  \left|
    \frac{(x - \overline{x}) \cdot (y-\overline{y})}{\norm{x-\overline{x}}\norm{y-\overline{y}}}
  \right|,
\]
kde $\overline{x}$ je stredná hodnota vektora $x$ a $x-\overline{x}$ je vektor, ktorého zložky sú zložkami vektora $x$ po odčítaní hodnoty $\overline{x}$, nazývame Pearsonovou mierou podobnosti, alebo skrátene iba Pearsonovou podobnosťou.

Všimnime si, že zobrazenie $\mathrm{mp}_2$ nie je dobre definované pre konštantné obrazy. Preto musíme rozšíriť definíciu tohto zobrazenia nasledovným spôsobom:
\[
  \mathrm{mp}^*_2(x,y)
  =
  \begin{cases}
    \mathrm{mp}_2(x,y), &\text{ak }x,y \not\in \mathbb{R}_{\mathrm{const}}^n, \\
    1, &\text{ak }x,y \in \mathbb{R}_{\mathrm{const}}^n, \\
    0, &\text{inak,}
  \end{cases}
\]
kde $\mathbb{R}_{\mathrm{const}}^n$ označuje podmnožinu množiny $\mathbb{R}^n$ pozostávajúcu iba z vektorov, ktorých všetky prvky majú rovnakú hodnotu.

\textbf{Definícia 3.3.}
Zobrazenie
\[
  \mathrm{mp}_3(x,y)
  =
  \left|
    \frac{x_\mathrm{ord} \cdot y_\mathrm{ord}}{\norm{x_\mathrm{ord}}\norm{y_\mathrm{ord}}}
  \right|
\]
nazývame Spearmanovou mierou podobnosti. Pre vektor $x$ je vektor $x_\mathrm{ord}$ definovaný ako poradie tej-ktorej zložky vektora $x$.

V tomto prípade môže nastať problém s vektormi, ktoré majú viacero zložiek s~rovnakou hodnotou. V takomto prípade za poradie daného čísla považujeme priemernú hodnotu poradí, ktoré nadobudlo toto číslo v usporiadaní.

\textbf{Definícia 3.4.}
Zobrazenie
\[
  \mathrm{mp}_4(x,y)
  =
  \frac{2}{n(n-1)}
  \left|
    \sum_{j=1}^n
    \sum_{i=1}^{j-1}
    \mathrm{sgn}(x_i - x_j)
    \,
    \mathrm{sgn}(y_i - y_j)
  \right|,
\]
kde $\mathrm{sgn} \colon \mathbb{R} \to \{-1,0,1\}$ je zobrazenie dané predpisom
\[
  \mathrm{sgn}(x)
  =
  \begin{cases}
    1, &\text{ak }x > 0, \\
    0, &\text{ak }x = 0, \\
    -1, &\text{ak }x < 0,
  \end{cases}
\]
nazývame Kendallovou mierou podobnosti.

Nasledujúca veta hovorí o tom, že tieto zobrazenia sú skutočne mierami podobnosti.

\textbf{Veta 3.1.}
Zobrazenia $\mathrm{mp}_1$, $\mathrm{mp}_2$, $\mathrm{mp}_3$ a $\mathrm{mp}_4$ spĺňajú podmienky (MP1) až (MP3) z definície 3.1 a sú teda mierami podobnosti v tomto zmysle.

\textsc{Dôkaz.}
V prvom rade musíme ukázať, že dané zobrazenia nadobúdajú hodnoty z intervalu $[0,1]$. Keďže všetky korelačné koeficienty, na ktorých sú založené tieto zobrazenia, nadobúdajú hodnotu z~intervalu $[-1,1]$, tak ich absolútna hodnota sa nachádza v intervale $[0,1]$. Symetrickosť vyplýva automaticky z toho, že aj uvažované korelačné koeficienty sú symetrické. Ostatné vlastnosti sú tiež ľahko overiteľné, ukážme napríklad, že platí (MP3) pre $\mathrm{mp}_2$. Pozorujme, že
\begin{align*}
  \mathrm{mp}_2(x,\lambda y)
  &=
  \left|\frac{(x-\overline{x})\cdot(\lambda y-\overline{\lambda y})}{\norm{x-\overline{x}}\norm{\lambda y-\overline{\lambda y}}}\right|
  =
  \left|\frac{\lambda(x-\overline{x})\cdot(y-\overline{ y})}{\norm{x-\overline{x}}\lambda\norm{y-\overline{y}}}\right|
  \\
  &=
  \left|\frac{(x-\overline{x})\cdot(y-\overline{ y})}{\norm{x-\overline{x}}\norm{y-\overline{y}}}\right|
  =
  \mathrm{mp}_2(x,y).
\end{align*}
Tým sme ukázali, že vlastnosť (MP3) platí pre $\mathrm{mp}_2$. Rovnakým spôsobom to môžeme ukázať aj pre $\mathrm{mp}_1$, $\mathrm{mp}_3$ a $\mathrm{mp}_4$.
\hfill $\square$

\section{Dosiahnuté výsledky}
V tejto časti popíšeme dosiahnuté výsledky pri použití oboch aproximačných algoritmov. Najskôr potrebujeme charakterizovať slovník $D \in \mathbb{R}^{n \times d}$. My sme si zvolili Haar slovník, ktorý je definovaný v publikácii \cite{}.

Pre tento slovník platí $d = 7n^2$. Vizualizáciu slovníku pre obrazy veľkosti $6 \times 6$ je možné nájsť na obrázku \ref{fig:haar6} na strane \pageref{fig:haar6}.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=4cm]{figures/bell.png}
  
  \caption{Obraz veľkosti $256 \times 256$, na ktorom sme testovali matching-pursuit a orthogonal matching-pursuit algoritmus. Tento obraz sme jednotlivo rozdelili na menšie segmenty veľkosti $8 \times 8$, ktoré reprezentujú náš vektor $y$.}
  \label{fig:orig}
\end{figure}

\begin{figure}[p]
  \centering
  \includegraphics[width=0.925\linewidth]{figures/haar-slovnik/haar-6-slovnik.pdf}
  
  \caption{Haar slovník pre obrazy veľkosti $6 \times 6$, teda $n = 36$. Tento slovník pozostáva z $d = 7n^2 = 252$ obrazov.}
  \label{fig:haar6}
\end{figure}

Uvažované segmenty obrazu (obr.\ \ref{fig:orig}) sú veľkosti $8 \times 8$ pixelov a preto na presnú interpretáciu týchto segmentov potrebujeme 64 čísel. Pri testovaní matching-pursuit algoritmu sme vyskúšali reprezentáciu pomocou $L=50$, $100$, $150$, $200$ a $250$ obrazov zo slovníka. Bohužiaľ tieto výsledky nevedú k uspokojivej reprezentácii obrazu, ako je možné vidieť na obrázkoch 3.3 až 3.7.

Všimnime si, že už pri $L=50$ potrebujeme uchovať $2L = 100$ čísel, to je o 36 čísel viac než je potrebných na presnú reprezentáciu -- ide o pozície obrazov vybraných zo slovníka na danú reprezentáciu a k nim ich prislúchajúce koeficienty. Tým pádom použitie matching-pursuit algoritmu je pre praktické účely nezaujímavé.

Keďže každá iterácia matching-pursuit algoritmu zaberie rovnako dlhý čas a teda pracuje oproti orthogonal matching-pursuit algoritmu oveľa rýchlejšie, mohol by nájsť uplatnenie v iných oblastiach než sú nadbytočné a riedke reprezentácie obrazu. Napriek svojej nedokonalosti, tento algoritmus zachováva podstatu obrazu, a je teda možné ľudským okom rozoznať, čo by sa na danom obraze mohlo nachádzať. Toto by mohlo viesť k ďalšiemu spôsobu zašumenia dát pre potreby testov overovania ľudských bytostí -- test typu ,,potvrďte, že nie ste robot''.

Pri orthogonal matching-pursuit algoritme sme použili na reprezentáciu $L=5$, $10$, $15$, $20$ a $25$ obrazov zo slovníka. Výsledky pre jednotlivý počet obrazov si je možné pozrieť na obrázkoch 3.8 až 3.12. Klasická kosínusová transformácia už pri použití $L=5$ obrazov zo slovníka generuje dostatočne rozpoznateľný obraz, pričom ostatné uvažované miery podobnosti v daných segmentoch ukrývajú podstatnú časť informácie.

Pri použití $L=10$ obrazov vychádza ako najlepší kandidát stále miera podobnosti založená na kosínusovej transformácii, no pri $L=15$ je vidieť jemne lepšie výsledky pri použití iných mier podobností ako je kosínusová transformácia -- viď napríklad postava sediaca pod srdcom zvona.

Ako zaujímavé považujeme, že miery podobnosti $\mathrm{mp}_3$ a $\mathrm{mp}_4$, teda miery založené na Spermanovom a Kendallovom korelačnom koeficiente, vedú pri malej hodnote $L$ k identickým reprezentáciám. Tento výsledok ale berieme skôr ako zhodu okolností pre nami zvolený testovací obraz.

\newpage

\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp1-50.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp2-50.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp3-50.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp4-50.png}
  
  \caption{Výsledky matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=50$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}
\vfill
\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp1-100.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp2-100.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp3-100.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp4-50.png}
  
  \caption{Výsledky matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=100$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}

\newpage

\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp1-150.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp2-150.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp3-150.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp4-50.png}
  
  \caption{Výsledky matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=150$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}
\vfill
\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp1-200.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp2-200.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp3-200.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp4-50.png}
  
  \caption{Výsledky matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=200$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}

\newpage

\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp1-250.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp2-250.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp3-250.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-mp/mp-mp4-50.png}
  
  \caption{Výsledky matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=250$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}
\vfill
\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp1-5.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp2-5.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp3-5.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp4-5.png}
  
  \caption{Výsledky orthogonal matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=5$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}

\newpage

\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp1-10.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp2-10.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp3-10.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp4-10.png}
  
  \caption{Výsledky orthogonal matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=10$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}
\vfill
\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp1-15.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp2-15.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp3-15.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp4-15.png}
  
  \caption{Výsledky orthogonal matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=15$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}

\newpage

\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp1-20.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp2-20.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp3-20.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp4-20.png}
  
  \caption{Výsledky orthogonal matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=20$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}
\vfill
\begin{figure}[ht!]
  \centering
  
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp1-25.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp2-25.png}
  
  \vspace{0.1cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp3-25.png}
  \hspace{0.5cm}
  \includegraphics[width=4cm]{figures/vysledok-omp/omp-mp4-25.png}
  
  \caption{Výsledky orthogonal matching-pursuit algoritmu pre segmenty veľkosti $8 \times 8$ pri~použití $L=25$ obrazov zo slovníka pri použití rôznych mier podobností.}
\end{figure}
\chapter{Záver}

V tejto bakalárskej práci sme preskúmali základné algoritmy odvetvia matematiky s názvom ,,Riedke a nadbytočné reprezentácie''. Špeciálnejšie sme sa oboznámili ako aproximačne riešiť úlohu
\begin{center}
  \begin{tabular}{rl}
    minimalizuj & $\norm{y - Dx}_2$ \\
    za podmienky & $\norm{x}_0 \leq L$
  \end{tabular}
\end{center}
kde $x$ je uvažovaný obraz, ktorého reprezentáciu hľadáme, $D$ je slovník obrazov, ktoré na reprezentáciu obrazu $x$ používame a $\alpha$ je vektor koeficientov hovoriaci o tom, ako veľmi ten-ktorý obraz zo slovníka použijeme na reprezentáciu obrazu $x$. Číslo $L$ označuje najväčší počet obrazov, ktoré chceme zo slovníka použiť.

Najčastejšie používané algoritmy na hľadanie aproximácie riešenia tejto úlohy sú matching-pursuit a orthogonal matching-pursuit algoritmy. Oba algoritmy sme naprogramovali a otestovali.

Matching-pursuit algoritmus neviedol k najlepším výsledkom, ale stále môže byť využiteľný v iných oblastiach matematiky, respektíve iných oblastiach informatiky. Orthogonal matching-pursuit algoritmus viedol k lepším a použiteľnejším výsledkom.

Pri oboch algoritmoch sme navyše implementovali iný spôsob výberu obrazov zo slovníka. Pôvodne oba algoritmy využívajú mieru podobnosti založenú na~kosínusovej transformácii. V práci prezentujeme ďalšie tri miery podobnosti, ktoré sú založené na Pearsonovom, Spearmanovom a Kendallovom korelačnom koeficiente. Úprava algoritmov bola rovnako implementovaná a výsledky sú prezentované v práci.

\chapter*{Použité symboly}
\addcontentsline{toc}{chapter}{Použité symboly}

\noindent
\begin{tabular}{p{0.15\linewidth} p{0.75\linewidth}}
  $\N$ & množina prirodzených čísel $\{1,2,\dots\}$ \\
  $\Z$ & množina celých čísel $\{0, \pm1, \pm2, \dots\}$ \\
  $\Q$ & pole racionálnych čísel \\
  $\R$ & pole reálnych čísel \\
  $\R^n$ & $n$-rozmerný vektor reálnych čísel \\
  $\overline{x}$ & stredná hodnota vektora $x$ \\
  $x_\mathrm{ord}$ & vektor poradí hodnôt vektora $x$ \\
  $x \cdot y$ & skalárny súčin vektorov $x$ a $y$ \\
  $\norm{x}_2$ & norma vektora $x$ definovaná ako $\norm{x}=\sqrt{x \cdot x}$ \\
  $\norm{x}_0$ & počet nenulových prvkov vektora $x$ \\
  $\mathrm{mp}_1$ & miera podobnosti založená na kosínusovej podobnosti \\
  $\mathrm{mp}_2$ & miera podobnosti založená na Pearsonovom koeficiente \\
  $\mathrm{mp}_2^*$ & rozšírenie miery podobnosti založenej na Pearsonovom korelačnom koeficiente \\
  $\mathrm{mp}_3$ & miera podobnosti založená na Spearmanovom koeficiente \\
  $\mathrm{mp}_4$ & miera podobnosti založená na Kendallovom koeficiente \\
  $\mathrm{sgn}$ & znamienková funkcia
\end{tabular}

\cleardoublepage
\addcontentsline{toc}{chapter}{\listfigurename}
\listoffigures

\cleardoublepage
\addcontentsline{toc}{chapter}{\listtablename}
\listoftables

\cleardoublepage
\addcontentsline{toc}{chapter}{Literatúra}
\begin{thebibliography}{xxx}
  \bibitem{uvodKniha} Gonzalez, R. C., Woods, R. E.: Digital Image Processing. 2nd ed. Prentice Hall, (2002). ISBN: 9780201180756

  \bibitem{sparseKniha} Elad, M.: Sparse and redundant representations: from theory to applications in signal and image processing. Springer, (2010). ISBN: 9781441970107

  \bibitem{clanok1} Elad, M.: Sparse and redundant representation modeling---What next? IEEE Signal Processing Letters 19(12), 922--928, (2012).
  
  \bibitem{clanok2} Elad, M., Figueiredo, M. A., Ma, Y.: On the role of sparse and redundant representations in image processing. Proceedings of the IEEE 98(6), 972--982, (2010).
  
  \bibitem{mozno} Shinde, B. S., Dani, A. R.: The origins of digital image processing \& application areas in digital image processing. IOSR Journal of Engineering 1(1), 66--71, (2011).

  \bibitem{clanok3} Kumar, A., Shaik, F.: Image Processing in Diabetic Related Causes. Springer, (2016). ISBN: 9789812876232  
  
  \bibitem{clanok4} Yang, J., Peng, Y., Xu, W., Dai, Q.: Ways to sparse representation: An overview 52(4), 695--703, (2009).

\end{thebibliography}































\end{document}
